// 1. Get all items that are available

function getAvailableItems(items) {
    return items.filter(item => item.available);
}

module.exports = getAvailableItems;