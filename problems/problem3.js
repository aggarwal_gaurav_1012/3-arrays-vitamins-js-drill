// 3. Get all items containing Vitamin A.

function getItemsWithVitaminA(items) {
    return items.filter(items => items.contains.includes("Vitamin A"));
}
module.exports = getItemsWithVitaminA;