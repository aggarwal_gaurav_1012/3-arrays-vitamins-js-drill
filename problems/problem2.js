// 2. Get all items containing only Vitamin C.

function getItemsWithOnlyVitaminC(items) {
    return items.filter(item => item.contains === "Vitamin C");
}
module.exports = getItemsWithOnlyVitaminC;