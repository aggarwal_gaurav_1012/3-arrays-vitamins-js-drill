// 5. Sort items based on number of Vitamins they contain.

function countVitamins(items) {
    if (!items.contains) 
        return 0;
    return items.contains.split(',').length;
}

function sortItemsByVitamins(items) {
    return items.sort((a, b) => countVitamins(b) - countVitamins(a));
}

module.exports = sortItemsByVitamins;