// 4. Group items based on the Vitamins that they contain in the following format:
//     {
//         "Vitamin C": ["Orange", "Mango"],
//         "Vitamin K": ["Mango"],
//     }
//     and so on for all items and all Vitamins.

function groupItemsByVitamins(items) {
    const groupedItems = {};

    for (const item of items) {
        const vitamins = item.contains.split(", ");

        for (const vitamin of vitamins) {
            if (!groupedItems[vitamin]) {
                groupedItems[vitamin] = [];
            }
            groupedItems[vitamin].push(item.name);
        }
    }

    return groupedItems;
}

module.exports = groupItemsByVitamins;