const getItemsWithVitaminA = require('../problems/problem3');

const items = require('../data');

const result = getItemsWithVitaminA(items);

console.log("Items containing Vitamin A: ",result);