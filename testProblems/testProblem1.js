const getAvailableItems = require('../problems/problem1');

const items = require('../data');

const result = getAvailableItems(items);

console.log("Items that are available: ",result);