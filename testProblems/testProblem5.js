const sortItemsByVitamins = require('../problems/problem5.js');

const items = require('../data');

const sortItems = sortItemsByVitamins(items);

console.log("Items sort based on vitamins: ",sortItems);