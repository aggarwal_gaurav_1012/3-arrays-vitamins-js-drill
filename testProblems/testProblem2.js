const getItemsWithOnlyVitaminC = require('../problems/problem2');

const items = require('../data');

const result = getItemsWithOnlyVitaminC(items);

console.log("Items containing only Vitamin C: ",result);