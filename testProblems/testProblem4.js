const groupItemsByVitamins = require('../problems/problem4.js');

const items = require('../data');

const groupedItems = groupItemsByVitamins(items);

console.log("Items grouped based on vitamins: ",groupedItems);